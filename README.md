# Vue3 user list project


## Step1: Dowlload this project and go to directory to this project folder
```
git clone https://gitlab.com/vue3_project/userlist_project.git

cd userlist_project
```

## Step2: Project setup and installation
```
npm install
```

## Step3: Compiles and hot-reloads for development
```
npm run serve
```

## Step4: Use below link to open this web page on your brower
```
http://localhost:8080/
```
